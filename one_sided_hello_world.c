#include <mpi.h>
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    MPI_Init(NULL, NULL);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    char* buff;
    MPI_Win win;
    MPI_Win_allocate(10, MPI_CHAR, MPI_INFO_NULL, MPI_COMM_WORLD, &buff, &win);

    MPI_Win_fence(0, win);
    if (world_rank == 0)
        MPI_Put("Hello MPI\0", 10, MPI_CHAR, 1, 0, 10, MPI_CHAR, win);
    MPI_Win_fence(0, win);

    if (world_rank == 1)
        cout << "recieved " << buff;
    MPI_Finalize();
}
