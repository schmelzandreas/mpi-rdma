#include <mpi.h>
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    MPI_Init(NULL, NULL);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (world_rank == 0) {
        MPI_Send("Hello MPI\0", 10, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
    } else {
        char* buff = (char*)malloc(10);
        MPI_Recv(buff, 10, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        cout << "recieved "<< buff;
    }
    // Finalize the MPI environment.
    MPI_Finalize();
}
