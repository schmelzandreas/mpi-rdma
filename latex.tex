% This is based on the LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%
% See http://www.springer.com/computer/lncs/lncs+authors?SGWID=0-40209-0-0-0
% for the full guidelines.
%
\documentclass{llncs}
\usepackage[german, english]{babel}
\usepackage{courier}

\usepackage[utf8]{inputenc}
\usepackage[inline]{enumitem}

\usepackage{listings}
\usepackage{color}
\usepackage{subfig}
\usepackage{graphicx}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ %
  backgroundcolor=\color{white},   % choose the background color
  basicstyle=\footnotesize\ttfamily,
  breaklines=true,                 % automatic line breaking only at whitespace
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  keywordstyle=\color{blue},       % keyword style
  stringstyle=\color{mymauve},     % string literal style
  numbers=left,
   xleftmargin=.25in,
}


\begin{document}

\title{RDMA in MPI 3.x - Specification and Implementation}
%
\titlerunning{RDMA in MPI }  % abbreviated title (for running head)
%                                     also used for the TOC unless
%                                     \toctitle is used
%
\author{Andreas Schmelz}
%
\authorrunning{Schmelz A.} % abbreviated author list (for running head)
%
%%%% list of authors for the TOC (use if author list has to be modified)
\tocauthor{Andreas Schmelz}
%
\institute{Technische Universität München, Germany\\
\email{andreas.schmelz@tum.de}
}

\maketitle              % typeset the title of the contribution

\begin{abstract}
The Message passing Interface (MPI) is the de-facto standard for developing distributed shared memory applications. MPI’s traditional explicit message passing communication is the basis for many applications, yet this approach adds unwanted overhead. MPI’s one sided communication model was designed to exploit remote direct memory access (RMA) networks more efficiently.  One-sided communication on RMDA promises low latency and high scaling, but also raises challenges for correct synchronization and truly more efficient implementation in practice.

In this report we will explain one-sided communication in MPI 3.x. We will show its challenges, how they can be solved and how one-sided communication can simplify orchestration of communication.
Finally we will evaluate how MPI one sided communication is implemented in MPICH and present benchmarks of ported applications from two- to one sided communication

\keywords{Message Passing Interface (MPI), Remote Direct Memory Access (RDMA), one-sided communication, High Performance Computing (HPC)}
\end{abstract}
%
\section{Models of Parallel Programming}
%
There exist three programming models for parallel computing.
\begin{enumerate}
  \item In a shared memory model communication is done implicit by load and stores to memory, synchronization is explicit.
  \item In a globally distributed memory system communication is done explicit by passing messages and synchronization is implicitly achieved, as of every delivered message implies an acknowledgment.
  \item In a partitioned global address space (PGAS) model communication and synchronization is decoupled. Communication is done through remote memory accesses (RMA) and synchronization is managed separately.
\end{enumerate}
\cite{Hoefler}

Approaches one and two are widely known among computer science student. Popular libraries for these models are openMP for the shared memory model and MPI for message passing. A global address space approach is relatively new. Yet in large clusters and supercomputers the trend of network topology goes towards remote direct memory access (RDMA). This is the case, because the topology offers high bandwidth and low latency, as well as operating system bypass. In order to make use of RDMA, MPI introduced one sided communication in version 2.0. The initial release of MPI RMA received positive, as well as negative feedback\cite{Bonachea}\cite{Santh}. This feedback had been used to revise and extend MPI one-sided communication in version 3.0, released 2012. Particular the usage of hardware performance features, if available, was improved.

% MPI two sided
\subsection{MPI Two-Sided Communication}
% TODO: reduce
The traditional approach of communication is a two sided model. Two processes directly exchange messages in a point to point network. Every process within a communication group can send to and receive from every other, but perform only one of these actions at a time. The \texttt{MPI\_Send/ MPI\_Recv} functions are available in blocking and a non-blocking (asynchronous) variations. For communication sending and receiving process both must issue their intends.

This means that the communication step has to be known upfront which processes will send to which processes at which time. This communication order has to be static, or additionally globally communicated during runtime. This leads to a more sequential program execution order and will reduce parallel performance and complicate computations/ communication overlapping. For static scenarios this programming model might be sufficient. On the other side dynamic problems or problems with unpredictable, irregular communication patterns are harder to implement with two sided communication. Both target and origin needs to know there will happen a communication and also have to agree on a set order.

%
\section{Remote Direct Memory Access}
%
RDMA means that memory of two machines that are connected over a network interconnect (NIC) are able to share and access memory of each other respectively. These accesses do not have to call the operating system. This leads to lower latency, higher bandwidth and less CPU usage on both the senders and the receiver side. But it also requires special hardware that is more expensive than commodity Ethernet cards. In supercomputers mostly InfiniBand and CRAY networks are deployed. In 47\% of the Top 500 supercomputers InfiniBand is in use\cite{Infi}.

RDMA allows the use of the partitioned global address space (PGAS) programming model. In this programming model a program structures memory into continuous blocks called partitions. These partitions can be private, only the process can access and alter this memory, or public which allows also other processes to perform load and store operations on this partition. PGAS has been used to implement similar libraries to MPI like Unified Parallel C, Coarray Fortran and SHMEM.

From a Non-uniform memory access (NUMA) perspective remote memory becomes a layer along with local main memory as it still can be addressed over a global address.

The public accessible memory regions together reassemble cache-only memory architecture (COMA). In comparison to Non-uniform memory access (NUMA) COMA does not have a single global address, but each node does have some of the globally shared data. This approach forbids multiple redundant copies at the same time, but rather information has to be exchanged or data be migrated.

The top 10 Supercomputers as the state of November 2015 5 of the top10 use a cray network.
RDMA in comparison to other network protocols does work on a granularity of messages instead of binary data.
Conclusively MPI RMA promises:

\begin{itemize*}
    \item less memory consumption
    \item less CPU utilization
    \item lower latency
    \item higher bandwidth throughput
    \item less redundant memory copies
    \item asynchronous communication
\end{itemize*}

%
\section{MPI One-sided}
%
MPI is the de-facto standard for writing parallel programs for distributed memory systems. The MPI standard itself is an interface definition that describes its semantics but not its underlying implementation how each of the calls to MPI has to work. As a result of this there are several MPI implementations that are independent of the software, Operating system, programming language and hardware and network is used to exchange information between participating processes. Open implementations are MPICH and openMPI. MPI Implementations are also provided by Hardware vendors (Intel, IBM, HP, Mellanox). Those are highly optimized to utilize their underlying hardware. Even supercomputers are shipped with an optimized version of MPI to exploit the used hardware and network topology\cite{BlueG}.
MPI is designed to be portable, hence a program can be run on different hardware or MPI implementations. It is also possible to run a single MPI program on heterogeneous architectures.


%
\subsection{One-Sided Communication}
%
In the one-sided communication model only one process needs to become active in order to perform a data exchange. This approach separates communication and synchronization. The programmer has to sense about those individually. This makes one-sided communication programming more complex in the first step, but also allows new constructs for concurrent communication and overlapping computation/ communication. Processes can receive (-or passively send via a remote get request) without interruption. Typical applications for one-sided communication are problems with unstructured or adaptive meshes.
The MPI specification does not require RDMA hardware for one sided communication, yet it will highly increase performance if available and RMA operations of MPI will be executed natively on hardware. One-sided communication could also be emulated by message passing. This implies, that the performance ranges of MPI implementations can change very drastically. Implementations will most likely increase over its releases, because of incremental optimization. Thus it is hard to compare performance benchmarks between different implementations, because performance depends on many factors like hardware, MPI implementation, software optimization.


\subsubsection{Memory Exposure}
%
MPI communication structures its programming model into groups of processes, in which they can communicate with each other directly. A process can be part of multiple groups.
RMA support does not imply, other processes are allowed to access and modify other processes memory by default. To allow RMA, each process must collectively expose a local memory region, so called windows.
The program remains in control over its private memory, but additionally can declare memory as public.
Communication can only happen inside the same group, RMA operations are only allowed to exposed windows and those also only during certain time frames, called epochs.

There are two different memory modes: separated and unified. Displayed in figure \ref{fig:memory_model}.

\begin{figure}[t]
  \begin{tabular}{ccc}
  \subfloat[Separated memory model]{\includegraphics[width=6cm]{cropped_sep.png}} &
  \subfloat[Unified memory model]{\includegraphics[width=6cm]{cropped_uniform.png}}
  \end{tabular}
  \caption{Separated and unified memory models\cite{Hoefler}}
  \label{fig:memory_model}
  \centering
\end{figure}

\begin{description}
\item [Separated]
Separated means shared global memory is independent of local private memory, therefor MPI makes no assumptions about cache coherency. The users program is responsible to ensure coherency and synchronization in software. This relaxed model gives a lot of freedom to the MPI implementation, as it does not require specialized hardware and hence is highly portable. Remote updates to a public exposed window may not be reflected directly on a private copy and the programmer has to synchronize these windows himself.
\item [Unified]
In the unified memory model, MPI ensures cache coherency. Exposed windows are part of a cache hierarchy. The MPI specification explicitly encourages implementations to make use of underlying hardware that ensure ordering and coherence. Updates to a window is coherent reflected in both private and public window which is in case of underlying supported hardware the same memory region. CC does not ensure consistency. If multiple processes write to the same remote memory region during an exposure epoch order is not specified by MPI. Even calls during one epoch are not ensured to be executed in the order they are issued. Without further synchronization or locks this could lead to inconsistent states of the windows.

The separated model is available in MPI since version 2.0. Version 3.0 introduced the unified memory model. Programming in a separated fashion will be upwards compatible. The unified model may be more convenient, but requires a MPI 3.0 implementation and hardware compatibility.
\end{description}

The MPI 3.0 standard allows four different ways to expose memory windows. The initialization functions to create a window are collective calls. This means all of the processes within a group must call this function. Sizes of windows can vary across different processes. If a process does not want to expose memory, it still has to make the call, but can specify size to be zero.
The legacy MPI 2.0 \texttt{MPI\_Win\_create} function creates a window object on top of already allocated memory. Especially for a high number of processes this results in large translation buffers. It is recommended not to use this, but one of the three MPI 3.0 Window creation functions.

\texttt{MPI\_Win\_allocate} allocates memory itself and tries to align the memory position across all processes.

\texttt{MPI\_Win\_create\_dynamic} creates a window handle without allocated memory. Processes can attach memory to this window dynamically.

\texttt{MPI\_Win\_allocate\_shared} creates a window object similar to \texttt{MPI\_Win\_create} but provides additional functionality for windows on local shared memory.

\begin{figure}[t]
\includegraphics[width=\textwidth]{window_alloc.png}
\caption{Window allocation\cite{Hoefler}}
\centering
\end{figure}

%
\subsubsection{Communication}
%
MPI offers three different communication operations. Basic operations like MPI\_Put and MPI\_Get that only involve one active process. The second type are more complex accumulates, with additional atomic guarantees. ALL MPI RMA Operations are non-blocking.
MPI\_Put copies a memory buffer from an origin to a target window. MPI\_Get specifies a remote memory location in a Window from where to fetch the data. In listing \ref{one-sided} and \ref{two-sided} we present a simple example program. It sends the string "Hello MPI" from process 0 to process 1, then prints it out.


\begin{lstlisting}[language=C, caption={MPI two-sided communication "hello MPI" program},label={two-sided}]
 MPI_Init(NULL, NULL);

int world_rank;
MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

if (world_rank == 0) {
    MPI_Send("Hello MPI\0", 10, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
} else {
    char* buff = (char*)malloc(10);
    MPI_Recv(buff, 10, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    cout << "received "<< buff;
}
MPI_Finalize();

\end{lstlisting}

Listing \ref{two-sided} shows a minimal two-sided MPI hello world example. It initializes MPI and sends the string "Hello MPI" from process 0 to process 1. Then prints out the received string. We do not need any synchronization as with the matching of \texttt{MPI\_Send} to \texttt{MPI\_Recv} we get implicit synchronization. It shows the strict matching of Send and receive operations

\begin{lstlisting}[language=C, caption={MPI one-sided communication "hello MPI" program}, label={one-sided}]
MPI_Init(NULL, NULL);

int world_rank;
MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

char* buff;
MPI_Win win;

MPI_Win_allocate(10, MPI_CHAR, MPI_INFO_NULL, MPI_COMM_WORLD, &buff, &win);

MPI_Win_fence(0, win);
    world_rank == 0 &&
    MPI_Put("Hello MPI\0", 10, MPI_CHAR, 1, 0, 10, MPI_CHAR, win);
MPI_Win_fence(0, win);
MPI_Barrier(MPI_COMM_WORLD);
world_rank == 1 &&
cout << "recieved " << buff;
MPI_Finalize();

\end{lstlisting}

The program in \ref{one-sided} does the same as in \ref{two-sided}, but uses one-sided communication model. \texttt{MPI\_Win\_fence} being a collective synchronization call, it has to be called by all processes, even process 1, which does not actively participate in the communication. The buffer on process one is undefined, as we never initialized it. We could also have set its size to zero bytes by using \texttt{10 * world\_rank} as size definition in Line 9

%
\subsubsection{Synchronization}
%
As MPI one sided communication introduces the concept of access and exposure epochs to windows. All MPI operations to a remote window must happen during such an epoch and are not allowed outside of these epochs. To start and end an epoch the MPI program needs to call synchronization functions that indicate start and end of an epoch. These calls block until all necessary resources are available. During an epoch processes can make an arbitrary RMA and local accesses to any processes. There is no ordering of accesses or delivery or progress of communication guaranteed until the end of the epoch. In MPI we distinguish between two different principles of window synchronization: active and passive.

\begin{description}
\item[Active Target Synchronization]
Similar to MPI two sided communication both target and origin have to become active and collectively start an epoch to a window. All processes of a communicator that hold a window must  start a epoch by calls to MPI\_Win\_fence. It behaves similar to MPI\_Barrier.  Only once all processes have issued that call, RMA communication can start. After the end of and epoch it is guaranteed that all RMA accesses are completed and we safely can locally access window memory without intercepting ongoing remote memory accesses. Active target Synchronization suits best for regular access patterns like stencil boundary exchange. But it also degrades parallel performance, as all processes have to synchronize. On entering an epoch all process have to wait for the slowest process to finish its local computation and on leaving all wait to finish their respective RMA operations.

\item[Passive Target Synchronization]
In passive target synchronization only the origin becomes active and accesses the target passively. It has to open an exposure epoch to a target. It does so by calling \texttt{MPI\_Win\_lock}/ \texttt{MPI\_Win\_unlock} with the target window as argument. The target process will not be notified about the foreign access, nor does it have to acknowledge the access. The lock can either be declared shared of exclusive. An exclusive epoch is only provided to a single process. Therefore RMA in an exclusive epoch is protected against interfering other remote accesses and its epoch calls are executed atomic, yet in undefined order.
With a shared lock multiple processes can access the same window at a time. \texttt{MPI\_Win\_lock} does not actually acquire a lock in a shared memory way and does not block. It start an epoch, buffers the accesses and executes them whenever the lock is provided. The only synchronization point in passive window synchronization is the \texttt{MP\_Win\_unlock}. At this point it is ensured that the RMA calls have finished, remotely and locally.

\begin{figure}[t]
\begin{tabular}{ccc}
\subfloat[active target synchronization]{\includegraphics[width=6cm]{cropped_active_target.png}} &
\subfloat[passive target synchronization]{\includegraphics[width=6cm]{cropped_passive.png}}
\end{tabular}
\caption{Window synchronization\cite{Hoefler}}
\centering
\end{figure}

\end{description}

MPI cannot guarantee that two remote processes write to the same window during the same synchronization epoch.
In MPI 2.0 such overlapping accesses were declared as erroneous behavior. This means it Was up to the MPI implementation to handle this. It could not only be the case that the program continues with undefined memory, but even terminate the program\cite{Bonachea}. This was criticized and was changed in MPI 3.0 to the more strict "undefined" outcome. The program must ensure local and remote read/ -writes do not overlap unsynchronized\cite{Cisco}.

Therefore the programmer needs to add additional synchronization during an epoch.

Synchronization can also be done by MPI two-sided communication calls like zero byte messages with \texttt{MPI\_Send} or a global \texttt{MPI\_barrier} to synchronizes all process. Yet this is possible and will achieve the desired effect, it has a significant slowdown.

A better alternative is MPI 3.0 newly introduced one-sided synchronization functions. \texttt{MPI\_Win\_sync} synchronizes local and remote memory of a window of a process. \texttt{MPI\_Win\_flush} completes the local outstanding communication of a window to a process. Both this functions are also as a "all" variation available, that executes the synchronization of the window to all processes. Note, that a similar effect could be achieved by closing and opening the epoch. This should be avoided as this is more expensive than the dedicated synchronization calls.


%
\section{Evaluation}
%
There are already many MPI one-sided implementations available. Mostly due to its young age, not many of them make use of the MPI 3.0 features nor are they tuned to the extend as their two-sided counterparts. Here we will  briefly explain MPICH's implementation architecture. Then we will present applications, that were ported from two-sided to one-sided communication.

\subsection{MPICH}
One of the most popular MPI implementations is MPICH (MPI Chameleon). It is highly performance optimized, open source and is used as the basis for many vendor specific MPI Implementations. MPICH is developed by an US organizations that apart from developing the core library, also researches and provides development tools in order to narrow the gap between hardware and software performance. Their implementation is the basis of many vendor adapted implementations like Cray, IBM, Intel, Mellanox and Microsoft.
Microsoft reached a Linpack efficiency of 90\% at 151 Terra Flops with their MPICH derived implementation using Windows Azure\footnote{https://www.mpich.org/static/docs/slides/2012-sc-bof/MPICH\_BOF\_SC12-Microsoft.pdf}.

MPICH'S implementation wants to support the contradicting goals of portability and high performance for programs and the MPI implementation itself. MPI's architecture is built in a way that many complex operations can be implemented by using lower level ones. With this principle MPI one sided communication and other complex functions can be implemented rapidly  by using two sided communication primitives. Starting there, operations can gradually be improved by implementing specialized communication primitives and harvesting hardware support\cite{Lusk}

Architecture wise this is achieved in MPICH by abstraction. All communication is propagated to a so called abstract device interface (ADI). MPICH has many different ADI implementations, that can implement their own queuing and buffering of messages to send and receive. The ADIs themselves allow portability, high performance and incremental improvements.

Usually an ADI is implemented by many different specialized devices. These different implementations are necessary to optimize complex functions and/- or use available hardware extensively. During runtime MPI calls are dispatched dynamically to the ADI and can differ based on the environment. This approach allows for reusing most of the upper level MPICH implementation code and ensures portability. E.g. small messages can be buffered  internally and larger messages may block, communication between processes on the same node is done by using shared memory copies, RDMA should be prioritized over TCP/IP.

The ADI is implemented by at least one channel Interface. This channel interface must implement primitive functions for MPI communication. The channel interface as the lowest level of implementation is very small, yet sufficient to model all other MPI functionality. This Channel Interface itself must be able to exchange data between two processes. The ADI then will use these functions. In such an architecture it is an complex question about which level has to ensure progress and which one is able to defer its execution by using buffers. MPICH makes no does not regulate if the ADI or the channel interface buffers. The data exchange in an channel interface can be done by three different protocols. In the Eager protocol, data is immediately send, even if there is no matching receive triggered yet, thus requires a large enough buffer on the receiver side. The rendezvous protocol sends data once both processes are in the state of send and receiving. The sender sends the receiver a control message indicating its intention to send a message. The get protocol specifies the receiver to directly copy memory from the sender. This is a very high-performing protocol, but requires supporting hardware. In case of a message exchange in a shared memory environment, this will be translated into a \texttt{memcpy} on the side of the receiver process\cite{Gropp}.

\subsection{FoMPI}
Fast one-sided MPI (foMPI) is a library that implemented the one-sided chapter of MPI for Cray Gemini and Aries systems. It was implemented to show, that MPI-3.0 RMA can be high performant, salable and only requiring a minimal memory footprint. Their work showed that it is a viable competitor against vendor-specific compiled languages. The foMPI implementation makes use of the lowest available API, closest to the hardware, in order to provide its goal of beating existing solutions.
\begin{figure}
  \includegraphics[width=\textwidth]{fo_latency.png}
  \caption{Latency comparison of Put and get operation\cite{Gerstenberger}}
  \label{fig:fo_latency}
  \centering
\end{figure}

Figure \ref{fig:fo_latency} shows, that foMPI outperforms cray implementations. Especially for small packages it shows, that foMPI provides the lowest latency. For small messages implementation overhead matters the most. In order to be fast for small and large packages, foMPI changes the used protocols depending on the package size. Small packages up to 4069  bytes use an optimized "fast-path" implementation, that only needs up to 200 instructions per operation. For larger messages this overhead matters less and the latency of one-sided implementations converges.

FoMPI is not only fast in micro benchmarks but also for real scientific applications. Gerstenberger et al. implemented production ready distributed memory applications and bench-marked those.

A distributed hash Table, that is an example for random load and stores. Their benchmark showed that MPI two-sided communication is competitive as long as communication happens within the same node, but decreases drastically once the number of processes surpasses the side of a single node.

A 3D fast Fourier transformation showed that foMPI is not only faster because of its close hardware implementation, but also provides better scaling. At Using 65536 processes foMPI delivers double the performance of MPI two-sided.

Third, a Lattice computation, that has a regular predictable communication pattern was implemented. FoMPi showed a consistent speedup over MPI-1 with an average of 13\%.
\cite{Gerstenberger}


\subsection{UPC port}
Shan H. has ported two real scientific applications from MPI two sided communication to one-sided communication in UPC. The applications consisting of over 70.000 lines of code needed less than 100 source code modifications.
Their initial naive one-sided communication implementation performed worse than their original one. After switching from global synchronization to one-sided synchronization performance increased drastically. Their UPC implementations outperformed the MPI two-sided one by a factor of 1.5. This effect was observed especially with an increasing number of processes. Shan H. showed that with modest modifications one-sided communication can contribute to a significant performance increase.
\cite{Shan}



%
\section{Conclusion}
%
With the version 3.0 the MPI forum improved the specification one-sided communication drastically. Semantics mostly stayed the same. Useful function call extensions were made, while keeping existing functions. Yet the very relaxed memory consistency model makes it easy to get conflicts in memory accesses. The two sided approach made it easy for programmers to rely on ordering of messages and implicit synchronization. Debugging parallel programs itself is inconvenient, RMA MPI programs add to this complexity\cite{Dinan}.

MPI performance in the one sided model mostly depends on the implementation of MPI. Researches showed, some MPI one-sided implementations lack behind their two-sided counterparts \cite{Santh}, others observed performance close to native communication interface\cite{Gropp}.

One sided communication introduced in MPI 2.0, that barely got attention by MPI implementations and HPC programmers tries to become more famous with good improvements. Yet it is still a comparatively new approach that needs to find adaption and mature MPI implementations. Knowledge and its programming paradigms needs to be acquired. This may take time as two sided communication is a competitor that is currently dominant in distributed shared memory systems. Complex MPI two-sided programs actively used will most likely not be rewritten, but there is a chance, that communication fragments are exchanged by one-sided communication to gain performance.

There are some challenges for MPI one-sided communication in order to become successful.
Errors in MPI programs are more dangerous and harder to debug than in the two sided model. When data races occur, the outcome is undefined. This can happen by multiple put/ get requests during the same epoch or local/ RMA memory accesses. Tools are already available that can provide static analysis, yet those will hardly capture all issues. It is hard to conclude about progress and completion in MPI without the correct use of synchronization. Getting this right is hard and therefore requires some sort of validation.

Two sided is not worse than the one-sided model and also should be picked wisely. The programming model should fit the problem. There are more years of research, optimization and programming experience in two-sided done. Sometimes a two-sided solution or simply more hardware can be the most cost effective solution. The strength of one-sided communication becomes more dominant on large scale.


While the MPI specification gives a rough estimation about a code structure, it does not yet answer how MPI programs can ensure correctness and high performance.
The portability of MPI programs is also a two sided sword. While it should allow the programmer to code independent of used hard- or software of the target machine, he might optimize for the hardware, which debugging and benchmarking is done. This will lead to hardware specific adjustments. In case this does not comply with the MPI standard the program could fail in production but not in the development environment. E.g. Infiniband issues messages the same order it receives them. An inexperienced developers could conclude from an observed behavior and wrong assumption about ordering in epochs.
Those issues will only fade over time. The dominance of existent RDMA supporting hardware and the ever increasing amount of processes in current supercomputers will make one-sided communication more popular. Experience, tools and implementation will make use of the available programming model, as it provides increased performance and simplifies composition of communication.

%
% ---- Bibliography ----
%
\begin{thebibliography}{5}
%
\bibitem {Cisco}
Squryes J.:
The New MPI-3 Remote Memory Access (One Sided) Interface

\bibitem{Hoefler}
Hoefler T. et al.:
Remote Memory Access Programming in MPI-3

\bibitem{Bonachea}
Bonachea D.
The Inadequacy of the MPI 2.0 One-sided Communication API for Implementing Parallel Global Address-Space Languages

\bibitem{Infi}
InfiniBand Continues its Position as the Preferred TOP500 Supercomputing Interconnect
InfiniBand Press Release

\bibitem{BlueG}
Almási G. et al.:
Implementing MPI on the BLueGene/L Supercomputer

\bibitem{Dinan}
Dinan J. et al.:
An Implementation and Evaluation of the MPI 3.0
One-Sided Communication Interface

\bibitem{Shan}
Shan H.:
Accelerating Applications at Scale Using One-Sided Communication

\bibitem{Santh}
Santhanaraman G.:
Natively Supporting True One-sided Communication in MPI on
Multi-core Systems with InfiniBand

\bibitem{Gropp}
Gropp W.:
A High-Performance, Portable Implementation of
the MPI Message Passing Interface Standard

\bibitem{Gerstenberger}
Enabling Highly-Scalable Remote Memory Access Programming with MPI-3 One Sided
Gerstenberger R. et al.

\bibitem{Lusk}
Lusk R.:
20 Years of MPICH
\end{thebibliography}


\end{document}
