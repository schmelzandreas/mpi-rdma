
#include <iostream>
#include <stdlib.h> 
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>
using namespace std;

struct cell {
  u_char cur:1;
  u_char tmp:1;
};



void step(cell* arr, int N) {
  for (int y = 0; y < N; y++) {
    for (int x = 0; x < N; x++) {
      // sum up all neighbours
      int sum = 0;
      for (int i = y-1; i <= y+1; i++) {
        for (int j = x-1; j <= x+1; j++) {
          if (i >= 0 && j >= 0 && i < N && j < N && !(i == x && j == y)) {
            if (arr[i * N + j].cur) {
              sum++;
            }
          }
        }
      }
      if (sum < 2 || sum > 3) {  // die
        arr[y * N + x].tmp = 0;
      } else if (sum == 3) {
        arr[y * N + x].tmp = 1;
      } else {
        arr[y * N + x].tmp = arr[y * N + x].cur;
      }
      //cout << "y: "<<y<<"x "<<x<<" is "<<sum << endl;
    }
  }
  // swap cur and tmp
  for (int i = 0; i < N*N; i++) {
    arr[i].cur = arr[i].tmp;
  }
  
}

void printArr(cell* arr, int N) {
  for (int y = 0; y < N; y++) {
    for (int x = 0; x < N; x++) {;
      cout << (int) arr[y * N + x].cur<< " ";
    }
    cout << endl;
  }
} 

void write(cell* arr, int N, const char* file) {
  
  ofstream outfile;
  outfile.open(file, std::ofstream::trunc);
  outfile << "P1" << endl;
  outfile << "#comment" << endl;
  outfile << N << " " << N << endl;

  for (int y = 0; y < N; y++) {
    for (int x = 0; x < N; x++) {
      outfile << (int) arr[y * N + x].cur<< " ";
    }
    outfile << endl;
  }
  outfile.close();
}

int main()
{
  // same sequence always
  srand((unsigned) 23942347);
  
  int N = 2000;
  int STEPS = 10;


  cell* arr = (cell*) malloc(N * N * sizeof(cell));


  for (int i = 0; i < N * N; i++) {
    arr[i].cur = (rand() % 1000) > 500 ? 1 : 0;
    arr[i].tmp = 0;
    
  }

  //printArr(arr, N);
  
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

  for (int i = 0; i < STEPS; i++) {
    
    
    //cout << "Step: "<<i <<endl;
    //printArr(arr, N);
    step(arr, N);


    //write(arr, N, ("out/"+ to_string(i) +".ppm").c_str());
    
  }
  std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
  //std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() <<std::endl;
  //std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() <<std::endl;

  double diff = (end - begin).count() / 1E9;
  cout << "took "<<diff << " = " << ((double)N*N*STEPS / 1E6) / diff << "Mups";

  write(arr, N, "out/out.ppm");
  free(arr);
  //write(arr, N, "end.ppm");

}

std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

