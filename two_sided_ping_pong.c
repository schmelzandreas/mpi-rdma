#include <mpi.h>
#include <stdio.h>
#include <stdlib.h> 

#include <iostream>
#include <stdlib.h> 
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>
#include <unistd.h>
#include <cmath>

using namespace std;
/*
mpicc hello.c
mpirun -n 4 ./a.out

mpic++ two_sided_ping_pong.c -std=c++11 && mpirun -n 4 ./a.out


*/


int main(int argc, char** argv) {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    int STEPS = 1000;
    int SIZE = 1000;

    void* buff = malloc(SIZE);
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Comm_size(comm, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(comm, &world_rank);

    for (int i = 0; i < STEPS; i++) {
      int proc_per_row = sqrt(world_size);
      if ((world_rank + i) % 2 == 0) {
        MPI_Send(buff, SIZE, MPI_BYTE, (world_rank + 1) % 2, 0, comm);
      } else {
        MPI_Recv(buff, SIZE, MPI_BYTE, (world_rank + 1) % 2, 0, comm, MPI_STATUS_IGNORE);
      }
    }
    
    // Finalize the MPI environment.
    MPI_Finalize();

    std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
    double diff = (end - begin).count() / 1E9;
    cout << "took "<< diff << endl;
}
