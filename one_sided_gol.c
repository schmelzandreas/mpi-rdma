#include <mpi.h>
#include <stdio.h>
#include <stdlib.h> 

#include <iostream>
#include <stdlib.h> 
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>
#include <unistd.h>
#include <cmath>

using namespace std;
/*
mpicc hello.c
mpirun -n 4 ./a.out

mpic++ hello.c -std=c++11 && mpirun -n 4 ./a.out


*/


struct cell {
  u_char cur:4; // TMP
  u_char tmp:1;
};


void printArr(cell* arr, int N) {
  for (int y = 0; y < N; y++) {
    for (int x = 0; x < N; x++) {;
      cout << (int) arr[y * N + x].cur<< " ";
    }
    cout << endl;
  }
} 

int getRank(int x, int y, int proc_per_row) {
  if (x < 0 || y < 0 || x >= proc_per_row || y >= proc_per_row) {
    return MPI_PROC_NULL;
  } else {
    return y * proc_per_row + x;
  }
}


void communicateDir(cell* arr, int proc_per_row, int world_rank, int rank_x, int rank_y, int N, int dir_x, int dir_y, char* buff, MPI_Win win) {

  int dst = getRank(rank_x + dir_x, rank_y + dir_y, proc_per_row);
  int src = getRank(rank_x - dir_x, rank_y - dir_y, proc_per_row);

  if (dst == MPI_PROC_NULL) return;

  if (dir_x == 1) {
    for (int i = 0; i < N; i++) {
      buff[i] = arr[i * N + N - 2].cur; // right inner element
    }
  // TODO this is false  MPI_Put(buff, N, MPI_CHAR, dst, world_rank * N, N, MPI_CHAR, win);
  } else if (dir_x == -1) {
    for (int i = 0; i < N; i++) {
      //buff[i] = arr[i * N + 1].cur; // left inner element
    // TODO this is slow
      MPI_Put(&arr[i * N + 1], 1, MPI_CHAR, dst, i * N, 1, MPI_CHAR, win);

    }
    //MPI_Put(buff, N, MPI_CHAR, dst, world_rank * N, N, MPI_CHAR, win);
  } else if (dir_y == 1) {
    MPI_Put(&arr[(N - 2) * N], N,  // src offset (one before last row)
      MPI_CHAR, dst, 0, // dst offset (top, first row)
      N, MPI_CHAR, win
    );
  } else if (dir_y == -1) {
    MPI_Put(&arr[1 * N], N,  // src offset (2nd row)
      MPI_CHAR, dst, (N - 1) * N, // dst offset (last row)
      N, MPI_CHAR, win
    );
  }
  
return;
  // MPI_PROC_NULL
  if (src != MPI_PROC_NULL) {
    if (dir_x == 1) {
    
      for (int i = 0; i < N; i++) {
        arr[i * N].cur = buff[i]; // left outer
      }
    } else if (dir_x == -1) {
      for (int i = 0; i < N; i++) {
        arr[i * N + N - 1].cur = buff[i]; // right outer
      }
    } else if (dir_y == 1) {
      for (int i = 0; i < N; i++) {
        arr[i].cur = buff[i]; // right outer
      }
    } else if (dir_y == -1) {
      for (int i = 0; i < N; i++) {
        arr[(N - 1) * N + i].cur = buff[i]; // bottom outer
      }
    }
  }
}

void communicate(cell* arr, int proc_per_row, int world_rank, int rank_x, int rank_y, int N, MPI_Win win) {
  char* buff = (char*) malloc(N);
  int src, dst, tmp;
  MPI_Win_fence(0, win);
  communicateDir(arr, proc_per_row, world_rank, rank_x, rank_y, N, 1, 0, buff, win);
  communicateDir(arr, proc_per_row, world_rank, rank_x, rank_y, N, -1,0, buff, win);
  communicateDir(arr, proc_per_row, world_rank, rank_x, rank_y, N, 0, 1, buff, win);
  communicateDir(arr, proc_per_row, world_rank, rank_x, rank_y, N, 0,-1, buff, win);
  MPI_Win_fence(0, win);
  MPI_Barrier(MPI_COMM_WORLD);
}


void step(cell* arr, int N) {
  for (int y = 1; y < N - 1; y++) {
    for (int x = 1; x < N - 1; x++) {
      // sum up all neighbours
      int sum = 0;
      for (int i = y-1; i <= y+1; i++) {
        for (int j = x-1; j <= x+1; j++) {
          if (i != x || j != y) {
            if (arr[i * N + j].cur) {
              sum++;
            }
          }
        }
      }
      if (sum < 2 || sum > 3) {  // die
        arr[y * N + x].tmp = 0;
      } else if (sum == 3) {
        arr[y * N + x].tmp = 1;
      } else {
        arr[y * N + x].tmp = arr[y * N + x].cur;
      }
      //cout << "y: "<<y<<"x "<<x<<" is "<<sum << endl;
    }
  }
  // swap cur and tmp
  for (int i = 0; i < N*N; i++) {
    arr[i].cur = arr[i].tmp;
  }
  
}

int main(int argc, char** argv) {
    
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    int proc_per_row = sqrt(world_size);
    
    int rank_x = world_rank % proc_per_row;
    int rank_y = world_rank / proc_per_row;
    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);


    srand((unsigned) world_rank);
  
    int N = 4000;
    int STEPS = 4000;
   
    cell* arr = (cell*) malloc(N * N * sizeof(cell)); 
    MPI_Win win;

    MPI_Win_create(arr, N * N, sizeof(char), MPI_INFO_NULL, MPI_COMM_WORLD, &win);


    for (int i = 1; i < N*N; i++) {
      arr[i].cur = (rand() % 1000) > 500 ? 1 : 0;
      arr[i].tmp = 0; 
    }
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    for (int i = 0; i < STEPS; i++) {
      //cout << "step "<<i<<endl;
      communicate(arr, proc_per_row, world_rank, rank_x, rank_y, N, win);
      //step(arr, N);
    }

     std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
 double diff = (end - begin).count() / 1E9;
  cout << "took "<<diff << " = " << ((double)(N-1)*(N-1)*STEPS / 1E6) / diff << "Mups";

    usleep(1000 * world_rank);

    cout << "proc "<<world_rank<<endl;
    //printArr(arr, N);

    // Print off a hello world message
    printf("Hello world from processor %s, rank %d"
           " out of %d processors\n",
           processor_name, world_rank, world_size);

    // Finalize the MPI environment.
    MPI_Finalize();
}
